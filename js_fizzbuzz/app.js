function fizzBuzz(iterations) {

    const ul = document.getElementById('list');
    ul.innerHTML = '';

    for(var i=1;i<=iterations;i++){
        
        const item = document.createElement('li');

        if((i%15) === 0) {
            
            console.log('Fizzbuzz');
            
            item.innerText = 'Fizzbuzz';
            ul.appendChild(item);

		} else if(i%3 === 0){
            
            console.log('Fizz');
            
            item.innerText = 'Fizz';
            ul.appendChild(item);

		} else if(i%5 === 0){
            
            console.log('Buzz');
            
            item.innerText = 'Buzz';
            ul.appendChild(item);

		} else {
            
            item.innerText = i;
            ul.appendChild(item);
        }
	}
}