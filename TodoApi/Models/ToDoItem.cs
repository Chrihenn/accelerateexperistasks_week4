using Newtonsoft.Json;
namespace TodoApi.Models
{
    public class TodoItem
    {
        public string TaskId { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
        public string Id{ get; set; }
    }
}