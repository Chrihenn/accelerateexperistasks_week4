using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;
using TodoApi.Models;
public class DocumentDbService
{
    private const string DatabaseName = "ToDoDb";

    private const string CollectionName = "ToDos";

    public async Task SaveDocumentAsync(TodoItem document)
    {
        try
        {
            var client = new DocumentClient(new Uri("https://cth-todoapi.documents.azure.com:443/"), "G3YqbKuwOtcgwjdQVCCBUP8BFavDpVlVPwQdnzLfClxzmMAPJIYZDTVpohwQqsiwBWOw320RhPvl7lFfuqnwzw==");
            await client.UpsertDocumentAsync(UriFactory.CreateDocumentCollectionUri(DatabaseName, CollectionName), document);
        }
        catch (Exception e)
        {
            Console.WriteLine("Error: {0}, Message: {1}", e.Message, e.GetBaseException().Message);
        }
    }

    public IQueryable<TodoItem> GetLatestDocuments()
    {
        try
        {
            var client = new DocumentClient(new Uri("https://cth-todoapi.documents.azure.com:443/"), "G3YqbKuwOtcgwjdQVCCBUP8BFavDpVlVPwQdnzLfClxzmMAPJIYZDTVpohwQqsiwBWOw320RhPvl7lFfuqnwzw==");
            return client.CreateDocumentQuery<TodoItem>(
                UriFactory.CreateDocumentCollectionUri(DatabaseName, CollectionName),
                "SELECT * FROM ToDos",
                new FeedOptions { 
                    MaxItemCount = 10,
                    EnableCrossPartitionQuery = true 
                    });
        }
        catch (Exception e)
        {
            Console.WriteLine("Error: {0}, Message: {1}", e.Message, e.GetBaseException().Message);
            return null;
        }
    }
}