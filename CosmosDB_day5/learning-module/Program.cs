﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Net;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;

namespace learning_module
{
    class Program
    {
        private DocumentClient client;
        static void Main(string[] args)
        {
            try
            {
                Program p = new Program();
                p.BasicOperations().Wait();
            }
            catch (DocumentClientException de)
            {
                Exception baseException = de.GetBaseException();
                Console.WriteLine("{0} error occurred: {1}, Message: {2}", de.StatusCode, de.Message, baseException.Message);
            }
            catch (Exception e)
            {
                Exception baseException = e.GetBaseException();
                Console.WriteLine("Error: {0}, Message: {1}", e.Message, baseException.Message);
            }
            finally
            {
                Console.WriteLine("End of demo, press any key to exit.");
                Console.ReadKey();
            }
        }

        private async Task BasicOperations()
        {
            this.client = new DocumentClient(new Uri(ConfigurationManager.AppSettings["accountEndpoint"]), ConfigurationManager.AppSettings["accountKey"]);

            await this.client.CreateDatabaseIfNotExistsAsync(new Database { Id = "Iste" });

            await this.client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri("Iste"), new DocumentCollection { Id = "Teas" });

            Console.WriteLine("Database and collection validation complete");

            Iste iste1 = new Iste
            {
                Id = "1",
                Brand = "Sunniva",
                Flavour = "Guava",
                Cost = 29.0
            };

        await this.CreateIsteDocumentIfNotExists("Iste", "Teas", iste1);
        await this.ReadUserDocument("Iste", "Teas", iste1);
        }

        private void WriteToConsoleAndPromptToContinue(string format, params object[] args)
        {
            Console.WriteLine(format, args);
            Console.WriteLine("Press any key to continue ...");
            Console.ReadKey();
        }

        private async Task ReadUserDocument(string databaseName, string collectionName, Iste iste)
        {
            try
            {
                await this.client.ReadDocumentAsync(UriFactory.CreateDocumentUri(databaseName, collectionName, iste.Id), 
                new RequestOptions { PartitionKey = new PartitionKey(iste.Id) });
                this.WriteToConsoleAndPromptToContinue("Read iste {0}", iste.Id);
            }
            catch (DocumentClientException de)
            {
                if (de.StatusCode == HttpStatusCode.NotFound)
                {
                    this.WriteToConsoleAndPromptToContinue("Iste {0} not read", iste.Id);
                }
                else
                {
                    throw;
                }
            }
        }

        private async Task CreateIsteDocumentIfNotExists(string databaseName, string collectionName, Iste iste)
        {
            try
            {
                await this.client.ReadDocumentAsync(UriFactory.CreateDocumentUri(databaseName, collectionName, iste.Id), 
                new RequestOptions { PartitionKey = new PartitionKey(iste.Id) });
                this.WriteToConsoleAndPromptToContinue("Iste {0} already exists in the database", iste.Id);
            }
            catch (DocumentClientException de)
            {
                if (de.StatusCode == HttpStatusCode.NotFound)
                {
                    await this.client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), iste);
                    this.WriteToConsoleAndPromptToContinue("Created iste {0}", iste.Id);
                }
                else
                {
                    throw;
                }
            }
        }
    }
}
