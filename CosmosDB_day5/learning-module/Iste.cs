using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;

namespace learning_module
{
    public class Iste {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("brand")]
        public string Brand { get; set; }
        
        [JsonProperty("flavour")]
        public string Flavour { get; set; }
        
        [JsonProperty("cost")]
        public double Cost { get; set; }


        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    
    }
}